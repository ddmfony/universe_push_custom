package com.comsince.github.util;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

public class JwtUtil {


    public static final long DEFAULT_EXPIRE_TIME = 7 * 24 * 60 * 60 * 1000;

    /**
     * 校验token是否正确
     * w
     *
     * @param token 密钥
     * @return 是否正确
     */
    public static boolean verify(String token, String account) {
        return verify(token, MD5Util.CIPHERTEXT, account);
    }

    public static boolean verify(String token, String secret, String account) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(account + secret);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withClaim("account", account)
                    .build();
            verifier.verify(token);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public static String getUserid(String token) throws JWTDecodeException {
        DecodedJWT jwt = JWT.decode(token);
        String userId = jwt.getClaim("account").asString();
        boolean verify = verify(token, userId);
        if (verify) {
            return userId;
        }
        return null;
    }

    /**
     * 获得Token中的信息无需secret解密也能获得
     *
     * @param token
     * @param claim
     * @return
     */
    public static String getClaim(String token, String claim) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim(claim).asString();
        } catch (JWTDecodeException e) {
            return null;
        }
    }


    /**
     * @param account
     * @param time
     * @param expireTime 过期时间 单位：毫秒
     * @return
     */
    public static String sign(String account, String secret, String time, long expireTime) {
        // 此处过期时间，单位：毫秒
        Date date = new Date(System.currentTimeMillis() + expireTime);
        Algorithm algorithm = Algorithm.HMAC256(account + secret);

        return JWT.create()
                .withClaim("account", account)
                .withClaim("currentTimeMillis", time)
                .withExpiresAt(date)
                .sign(algorithm);
    }


    public static String sign(String account, String time) {
        return sign(account, MD5Util.CIPHERTEXT, time, DEFAULT_EXPIRE_TIME);
    }

    public static String sign(String account, String secret, String time) {
        return sign(account, secret, time, DEFAULT_EXPIRE_TIME);
    }

    public static String getToken(ServletRequest request) {
        HttpServletRequest httpRequest = (HttpServletRequest)request;
        return httpRequest.getHeader("Token");

    }

}
