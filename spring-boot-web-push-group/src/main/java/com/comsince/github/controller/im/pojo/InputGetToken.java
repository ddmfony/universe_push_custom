package com.comsince.github.controller.im.pojo;

public class InputGetToken {
    private String userId;
    private String clientId;
//    private String token;

    public InputGetToken(String userId, String clientId) {
        this.userId = userId;
        this.clientId = clientId;
    }

//    public InputGetToken(String userId, String clientId, String token) {
//        this.userId = userId;
//        this.clientId = clientId;
//        this.token = token;
//    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

//    public String getToken() {
//        return token;
//    }

//    public void setToken(String token) {
//        this.token = token;
//    }
}
