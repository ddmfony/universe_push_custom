package com.comsince.github.handler.im;

import cn.wildfirechat.proto.ProtoConstants;
import cn.wildfirechat.proto.WFCMessage;
import com.comsince.github.common.ErrorCode;
import com.comsince.github.process.ImMessageProcessor;
import com.comsince.github.utils.MessageShardingUtil;
import io.netty.buffer.ByteBuf;

import java.util.Set;

import static cn.wildfirechat.proto.ProtoConstants.ContentType.Text;

@Handler(value = IMTopic.SendMessageTopic)
public class SendMessageHandler extends IMHandler<WFCMessage.Message> {
    private int mSensitiveType = 2;  //命中敏感词时，0 失败，1 吞掉， 2 敏感词替换成*。

    public SendMessageHandler() {
        super();
        mSensitiveType = 2;
    }

    @Override
    public ErrorCode action(ByteBuf ackPayload, String clientID, String fromUser, boolean isAdmin, WFCMessage.Message message, ImMessageProcessor.IMCallback callback) {
        ErrorCode errorCode = ErrorCode.ERROR_CODE_SUCCESS;
        if (message != null) {
            boolean ignoreMsg = false;
            if (!isAdmin) {  //admin do not check the right
                int userStatus = messageService.getUserStatus(fromUser);
                if (userStatus == 1 || userStatus == 2) {
                    return ErrorCode.ERROR_CODE_FORBIDDEN_SEND_MSG;
                }

                if (message.getConversation().getType() == ProtoConstants.ConversationType.ConversationType_Private) {
//                    if (messageService.isBlacked(message.getConversation().getTarget(), fromUser)) {
//                        return ErrorCode.ERROR_CODE_IN_BLACK_LIST;
//                    }
                    ErrorCode errorcode = messageService.checkFriend(fromUser, message.getConversation().getTarget());
                    if (errorcode != ErrorCode.ERROR_CODE_IS_FRIEND) {
                        return errorcode;
                    }
                }

                if (message.getConversation().getType() == ProtoConstants.ConversationType.ConversationType_Group && !messageService.isMemberInGroup(fromUser, message.getConversation().getTarget())) {
                    return ErrorCode.ERROR_CODE_NOT_IN_GROUP;
                } else if (message.getConversation().getType() == ProtoConstants.ConversationType.ConversationType_Group && messageService.isForbiddenInGroup(fromUser, message.getConversation().getTarget())) {
                    return ErrorCode.ERROR_CODE_NOT_RIGHT;
                } else if (message.getConversation().getType() == ProtoConstants.ConversationType.ConversationType_ChatRoom && !messageService.checkUserClientInChatroom(fromUser, clientID, message.getConversation().getTarget())) {
                    return ErrorCode.ERROR_CODE_NOT_IN_CHATROOM;
                } else if (message.getConversation().getType() == ProtoConstants.ConversationType.ConversationType_Channel && !messageService.checkUserInChannel(fromUser, message.getConversation().getTarget())) {
                    return ErrorCode.ERROR_CODE_NOT_IN_CHANNEL;
                }
            }

            long timestamp = System.currentTimeMillis();
            long messageId = MessageShardingUtil.generateId();
            message = message.toBuilder().setFromUser(fromUser).setMessageId(messageId).setServerTimestamp(timestamp).build();

            if (!isAdmin) {
                Set<String> matched = messageService.handleSensitiveWord(message.getContent().getSearchableContent());
                if (matched != null && !matched.isEmpty()) {
//                    messageService.storeSensitiveMessage(message);
                    if (mSensitiveType == 0) {
                        errorCode = ErrorCode.ERROR_CODE_SENSITIVE_MATCHED;
                    } else if (mSensitiveType == 1) {
                        ignoreMsg = true;
                    } else if (mSensitiveType == 2) {
                        String text = message.getContent().getSearchableContent();
                        for (String word : matched) {
                            text = text.replace(word, "***");
                        }

                        message = message.toBuilder().setContent(message.getContent().toBuilder().setSearchableContent(text).build()).build();
                    } else if (mSensitiveType == 3) {

                    }
                }
            }

//            boolean ignoreMsg = false;
            if (!isAdmin && message.getContent().getType() == Text) {
                message = message.toBuilder().setContent(message.getContent().toBuilder().setSearchableContent(message.getContent().getSearchableContent()).build()).build();
            }

            if (errorCode == ErrorCode.ERROR_CODE_SUCCESS) {
                if (!ignoreMsg) {
                    saveAndPublish(fromUser, clientID, message);
                }
                ackPayload = ackPayload.capacity(20);
                ackPayload.writeLong(messageId);
                ackPayload.writeLong(timestamp);
            }
        } else {
            errorCode = ErrorCode.ERROR_CODE_INVALID_MESSAGE;
        }
        return errorCode;
    }
}
