package com.comsince.github.handler.im;

import com.comsince.github.SubSignal;
import com.comsince.github.common.ErrorCode;
import cn.wildfirechat.proto.WFCMessage;
import com.comsince.github.process.ImMessageProcessor;
import io.netty.buffer.ByteBuf;

import static com.comsince.github.common.ErrorCode.ERROR_CODE_SUCCESS;

@Handler(value = IMTopic.DeleteFriendTopic)
public class DeleteFriendHandler extends IMHandler<WFCMessage.IDBuf> {
    @Override
    public ErrorCode action(ByteBuf ackPayload, String clientID, String fromUser, boolean isAdmin, WFCMessage.IDBuf request, ImMessageProcessor.IMCallback callback) {
        long[] head = new long[2];
        ErrorCode errorCode = messageService.deleteFriend(fromUser, request.getId());
        if (errorCode == ERROR_CODE_SUCCESS) {
            publisher.publishNotification(SubSignal.FN, fromUser, head[0]);
            publisher.publishNotification(SubSignal.FN, request.getId(), head[1]);
        }
        return errorCode;
    }
}
